pour debian/ubuntu 

sudo apt-get install git git-buildpackage debhelper gnupg
gpg --gen-key
gpg --list-keys
	/home/loginuser/.gnupg/pubring.gpg
	-----------------------------
	pub   ZZZZZ/XXXXXXXX 2016-04-15
	uid                  marsat <CTparental@laposte.net>
	sub   ZZZZZ/YYYYYYYY 2016-04-15

git config --global user.signingkey XXXXXXXX
git config --global user.name "marsat"
git config --global user.email CTparental@laposte.net
git clone https://gitlab.com/marsat/CTparental.git

mkdir $HOME/buildCT
rm -rf $HOME/buildCT/*
version="$(grep "ctparental (" -m 1 $(cd .; pwd)/debian/changelog  | cut -d"(" -f2 | cut -d"-" -f1)"

pi OS buster dnscrypt lighttpd:
cd CTparental/
git checkout master
git branch piOSbuster
git checkout piOSbuster
cp -f conf_lighttpd/conf_lighttpd_debian10 fonctions/confhttp
cp -f pi_os_debian10/* debian/
/bin/sed -i "s/debian-branch =.*/debian-branch = piOSbuster/g" debian/gbp.conf
git commit -a
gbp buildpackage
git checkout master
git branch -D piOSbuster
mv ../build-area/ctparental_${version}-1.0_all.deb $HOME/buildCT/ctparental_pi_OS_Buster_lighttpd_${version}-1.0_all.deb
rm -rf ../build-area/

debian11 lighttpd:
cd CTparental/
git checkout master
git branch debian11dnscrypt
git checkout debian11dnscrypt
cp -f conf_lighttpd/conf_lighttpd_debian10 fonctions/confhttp
cp -f ubuntu21.10/dist.conf debian/dist.conf
/bin/sed -i "s/debian-branch =.*/debian-branch = debian11dnscrypt/g" debian/gbp.conf
git commit -a
gbp buildpackage
git checkout master
git branch -D debian11dnscrypt
mv ../build-area/ctparental_${version}-1.0_all.deb $HOME/buildCT/ctparental_debian11_lighttpd_${version}-1.0_all.deb
rm -rf ../build-area/


debian11 lighttpd min:
cd CTparental/
git checkout min
git branch debian11dnscrypt
git checkout debian11dnscrypt
cp -f conf_lighttpd/conf_lighttpd_debian10 fonctions/confhttp
cp -f ubuntu21.10/dist.conf debian/dist.conf
/bin/sed -i "s/debian-branch =.*/debian-branch = debian11dnscrypt/g" debian/gbp.conf
git commit -a
gbp buildpackage
git checkout min
git branch -D debian11dnscrypt
mv ../build-area/ctparental_${version}-1.0_all.deb $HOME/buildCT/ctparental_debian11_lighttpd_min_${version}-1.0_all.deb
rm -rf ../build-area/


debian11 nginx:
cd CTparental/
git checkout master
git branch debian11dnscrypt
git checkout debian11dnscrypt
cp -f conf_nginx/confnginx1 fonctions/confhttp
cp -f ubuntu21.10nginx/* debian/
/bin/sed -i "s/debian-branch =.*/debian-branch = debian11dnscrypt/g" debian/gbp.conf
git commit -a
gbp buildpackage
git checkout master
git branch -D debian11dnscrypt
mv ../build-area/ctparental_${version}-1.0_all.deb $HOME/buildCT/ctparental_debian11_nginx_${version}-1.0_all.deb
rm -rf ../build-area/


debian11 nginx min:
cd CTparental/
git checkout min
git branch debian11dnscrypt
git checkout debian11dnscrypt
cp -f conf_nginx/confnginx1 fonctions/confhttp
cp -f ubuntu21.10nginx/* debian/
/bin/sed -i "s/debian-branch =.*/debian-branch = debian11dnscrypt/g" debian/gbp.conf
git commit -a
gbp buildpackage
git checkout min
git branch -D debian11dnscrypt
mv ../build-area/ctparental_${version}-1.0_all.deb $HOME/buildCT/ctparental_debian11_nginx_min_${version}-1.0_all.deb
rm -rf ../build-area/


debian10 ubuntu 18.10 nginx (car lighttpd bug avec les redirections sous firefox ):
cd CTparental/
git checkout master
git branch deb10nginx
git checkout deb10nginx
cp -f debian10nginx/* debian/
cp -f conf_nginx/confnginx1 fonctions/confhttp
/bin/sed -i "s/debian-branch =.*/debian-branch = deb10nginx/g" debian/gbp.conf
git commit -a
gbp buildpackage
git checkout master
git branch -D deb10nginx
mv ../build-area/ctparental_${version}-1.0_all.deb $HOME/buildCT/ctparental_debian10_nginx_${version}-1.0_all.deb
rm -rf ../build-area/

debian 10 lighttpd:
cd CTparental/
git checkout master
git branch debian10lighttpd
git checkout debian10lighttpd
cp -f debian10/* debian/
cp -f conf_lighttpd/conf_lighttpd_debian10 fonctions/confhttp
/bin/sed -i "s/debian-branch =.*/debian-branch = debian10lighttpd/g" debian/gbp.conf
git commit -a
gbp buildpackage
git checkout master
git branch -D debian10lighttpd
mv ../build-area/ctparental_${version}-1.0_all.deb $HOME/buildCT/ctparental_debian10_lighttpd_${version}-1.0_all.deb
rm -rf ../build-area/

debian 10 lighttpd min:
cd CTparental/
git checkout min
git branch deb10lighttpdMin
git checkout deb10lighttpdMin
cp -f debian10/* debian/
cp -f conf_lighttpd/conf_lighttpd_debian10 fonctions/confhttp
/bin/sed -i "s/debian-branch =.*/debian-branch = deb10lighttpdMin/g" debian/gbp.conf
git commit -a
gbp buildpackage
git checkout min
git branch -D deb10lighttpdMin
mv ../build-area/ctparental_${version}-1.0_all.deb $HOME/buildCT/ctparental_debian10_lighttpd_min_${version}-1.0_all.deb
rm -rf ../build-area/

debian10 ubuntu 18.10 nginx min:
cd CTparental/
git checkout min
git branch deb10nginxMin
git checkout deb10nginxMin
cp -f debian10nginx/* debian/
cp -f conf_nginx/confnginx1 fonctions/confhttp
/bin/sed -i "s/debian-branch =.*/debian-branch = deb10nginxMin/g" debian/gbp.conf
git commit -a
gbp buildpackage
git checkout master
git branch -D deb10nginxMin
mv ../build-area/ctparental_${version}-1.0_all.deb $HOME/buildCT/ctparental_debian10_nginx_min_${version}-1.0_all.deb
rm -rf ../build-area/

ubuntu20.04 lighttpd:
cd CTparental/
git checkout master
git branch ubuntu2004ligh
git checkout ubuntu2004ligh
cp -f conf_lighttpd/conf_lighttpd_debian10 fonctions/confhttp
cp -f ubuntu20.04/dist.conf debian/dist.conf
sed -i "s/lighttpd,/lighttpd,gamin,/g" debian/control
/bin/sed -i "s/debian-branch =.*/debian-branch = ubuntu2004ligh/g" debian/gbp.conf
git commit -a
gbp buildpackage
git checkout master
git branch -D ubuntu2004ligh
mv ../build-area/ctparental_${version}-1.0_all.deb $HOME/buildCT/ctparental_ubuntu20.04_lighttpd_${version}-1.0_all.deb
rm -rf ../build-area/

ubuntu20.04 nginx :
cd CTparental/
git checkout master
git branch ubuntu2004nginx
git checkout ubuntu2004nginx
cp -f ubuntu20.04nginx/* debian/
cp -f conf_nginx/confnginx1 fonctions/confhttp
/bin/sed -i "s/debian-branch =.*/debian-branch = ubuntu2004nginx/g" debian/gbp.conf
git commit -a
gbp buildpackage
git checkout master
git branch -D ubuntu2004nginx
mv ../build-area/ctparental_${version}-1.0_all.deb $HOME/buildCT/ctparental_ubuntu20.04_nginx_${version}-1.0_all.deb
rm -rf ../build-area/

ubuntu20.04 lighttpd min :
cd CTparental/
git checkout min
git branch ubuntu2004lighMin
git checkout ubuntu2004lighMin
cp -f conf_lighttpd/conf_lighttpd_debian10 fonctions/confhttp
sed -i "s/lighttpd,/lighttpd,gamin,/g" debian/control
cp -f ubuntu20.04/* debian/
/bin/sed -i "s/debian-branch =.*/debian-branch = ubuntu2004lighMin/g" debian/gbp.conf
git commit -a
gbp buildpackage
git checkout min
git branch -D ubuntu2004lighMin
mv ../build-area/ctparental_${version}-1.0_all.deb $HOME/buildCT/ctparental_ubuntu20.04_lighttpd_min_${version}-1.0_all.deb
rm -rf ../build-area/

ubuntu20.04 nginx min:
cd CTparental/
git checkout min
git branch ubuntu2004nginxMin
git checkout ubuntu2004nginxMin
cp -f ubuntu20.04nginx/* debian/
cp -f conf_nginx/confnginx1 fonctions/confhttp
/bin/sed -i "s/debian-branch =.*/debian-branch = ubuntu2004nginxMin/g" debian/gbp.conf
git commit -a
gbp buildpackage
git checkout master
git branch -D ubuntu2004nginxMin
mv ../build-area/ctparental_${version}-1.0_all.deb $HOME/buildCT/ctparental_ubuntu20.04_nginx_min_${version}-1.0_all.deb
rm -rf ../build-area/

###

ubuntu21.10 lighttpd:
cd CTparental/
git checkout master
git branch ubuntu2110ligh
git checkout ubuntu2110ligh
cp -f conf_lighttpd/conf_lighttpd_debian10 fonctions/confhttp
cp -f ubuntu21.10/* debian/
sed -i "s/lighttpd,/lighttpd,gamin,/g" debian/control
/bin/sed -i "s/debian-branch =.*/debian-branch = ubuntu2110ligh/g" debian/gbp.conf
git commit -a
gbp buildpackage
git checkout master
git branch -D ubuntu2110ligh
mv ../build-area/ctparental_${version}-1.0_all.deb $HOME/buildCT/ctparental_ubuntu21.10_lighttpd_${version}-1.0_all.deb
rm -rf ../build-area/

ubuntu21.10 nginx :
cd CTparental/
git checkout master
git branch ubuntu2110nginx
git checkout ubuntu2110nginx
cp -f ubuntu21.10nginx/* debian/
cp -f conf_nginx/confnginx1 fonctions/confhttp
/bin/sed -i "s/debian-branch =.*/debian-branch = ubuntu2110nginx/g" debian/gbp.conf
git commit -a
gbp buildpackage
git checkout master
git branch -D ubuntu2110nginx
mv ../build-area/ctparental_${version}-1.0_all.deb $HOME/buildCT/ctparental_ubuntu21.10_nginx_${version}-1.0_all.deb
rm -rf ../build-area/

ubuntu21.10 lighttpd min :
cd CTparental/
git checkout min
git branch ubuntu2110lighMin
git checkout ubuntu2110lighMin
cp -f conf_lighttpd/conf_lighttpd_debian10 fonctions/confhttp
sed -i "s/lighttpd,/lighttpd,gamin,/g" debian/control
cp -f ubuntu21.10/* debian/
/bin/sed -i "s/debian-branch =.*/debian-branch = ubuntu2110lighMin/g" debian/gbp.conf
git commit -a
gbp buildpackage
git checkout min
git branch -D ubuntu2110lighMin
mv ../build-area/ctparental_${version}-1.0_all.deb $HOME/buildCT/ctparental_ubuntu21.10_lighttpd_min_${version}-1.0_all.deb
rm -rf ../build-area/

ubuntu21.10nginx min:
cd CTparental/
git checkout min
git branch ubuntu2110nginxMin
git checkout ubuntu2110nginxMin
cp -f ubuntu21.10nginx/* debian/
cp -f conf_nginx/confnginx1 fonctions/confhttp
/bin/sed -i "s/debian-branch =.*/debian-branch = ubuntu2110nginxMin/g" debian/gbp.conf
git commit -a
gbp buildpackage
git checkout master
git branch -D ubuntu2110nginxMin
mv ../build-area/ctparental_${version}-1.0_all.deb $HOME/buildCT/ctparental_ubuntu21.10_nginx_min_${version}-1.0_all.deb
rm -rf ../build-area/
